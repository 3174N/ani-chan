# Ani-chan

[![Discord](https://img.shields.io/discord/867985175697567744)](https://discord.gg/5s69DV7gwP)

Ani-chan - A Discord bot for browsing AniList.

## Overview

AnilistBot is a bot for browsing AniList from within Discord.
You can search for anime and manga, compare server scores, and more.

To use the bot, you can:

- [Invite it to your server](https://discord.com/api/oauth2/authorize?client_id=861173907644743680&permissions=137439308864&scope=bot)
- [Run it yourself](#running-the-bot)
- [Join the Dev Server](https://discord.gg/5s69DV7gwP) and use it there

## Running the bot

See [INSTALLATION.md](INSTALLATION.md)

## License

The code in this repository is licensed under the [GPL-3 license](LICENSE), which basically allows you to do whatever you want with it.
Feel free to clone, fork, make changes to the code, etc'. PRs are welcome!
