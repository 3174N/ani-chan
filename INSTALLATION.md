# Running the bot on your own
## Setup
1. Create a `.token` file and put your bot token in it (see [.token.ex](.token.ex)).
2. Create a `config.json` file (see [config.json.ex](config.json.ex)).
3. Install dependencies by executing `pip install -r requirements.txt`
4. Run the bot by executing `python main.py`

_**NOTE:** If you plan to host the bot using a hosting service make sure it enables file saving. If it doesn't, use another service or change [files.py](files.py) however you see fit._
